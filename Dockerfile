FROM ubuntu:trusty
MAINTAINER Panos Mavrogiorgos otinanai90@yopmail.com

#ENV GOSU_VERSION 1.9
#RUN set -x \
    #&& dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
    #&& wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
    #&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc" \
    #&& export GNUPGHOME="$(mktemp -d)" \
    #&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
    #&& gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
    #&& rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc \
    #&& chmod +x /usr/local/bin/gosu

# install apt-fast
RUN apt-get update && \
    apt-get install \
        -y \
        --no-install-recommends \
        --no-install-suggests \
        software-properties-common=0.92.37.7 && \
    add-apt-repository -y ppa:saiarcot895/myppa && \
    apt-get update && \
    apt-get install \
        -y \
        --no-install-recommends \
        --no-install-suggests \
        apt-fast='1.8.3~144+git3a6bd77-0ubuntu1~ppa4~trusty1' && \
    apt-fast install \
        -y \
        --no-install-recommends \
        --no-install-suggests \
        build-essential=11.6ubuntu6 \
        libcurl4-openssl-dev=7.35.0-1ubuntu2.9 \
        libffi-dev=3.1~rc1+r3.0.13-12ubuntu0.1 \
        libssl-dev=1.0.1f-1ubuntu2.21 \
        libpq-dev=9.3.14-0ubuntu0.14.04 \
        tree=1.6.0-1 \
        libpcre++-dev=0.9.5-6 \
        git=1:1.9.1-1ubuntu0.3 \
        vim=2:7.4.052-1ubuntu3 \
        curl=7.35.0-1ubuntu2.9 \
        wget=1.15-1ubuntu1.14.04.2 \
        ruby=1:1.9.3.4 \
        python=2.7.5-5ubuntu3 \
        python-dev=2.7.5-5ubuntu3 \
        ca-certificates=20160104ubuntu0.14.04.1 \
        #python-pip=1.5.4-1ubuntu4 && \
        python-pip=1.5.4-1ubuntu4
    # cleanup
    #apt-get autoremove -y && \
    #apt-get clean && \
    #rm -rf /var/lib/apt/lists/* \
           #/var/tmp/* \
           #/tmp/*

# create the test-user
# We shouldn't hardcode the UID. using gosu is probably a better idea but anyway...
RUN useradd --shell /bin/bash -u 1000 -o -c "" -m test_user && \
    echo 'test_user ALL=(ALL) NOPASSWD: ALL' | tee -a /etc/sudoers && \
    echo 'gem: --no-rdoc --no-ri' | tee -a /root/.gemrc && \
    mkdir -p /cache/pip && \
    mkdir -p /cache/bundle && \
    mkdir -p /cache/embedded

COPY Gemfile /tmp/Gemfile
RUN gem install bundler
    #bundle install --deployment --path /cache/bundle --gemfile /tmp/Gemfile --binstubs /usr/local/bin

COPY requirements.txt /tmp/requirements.txt
COPY requirements-opt.txt /tmp/requirements-opt.txt
COPY requirements-test.txt /tmp/requirements-test.txt
RUN pip install --download-cache /cache/pip -U pip setuptools urllib3[secure]
RUN pip install --cache-dir /cache/pip -r /tmp/requirements.txt && \
    pip install --cache-dir /cache/pip -r /tmp/requirements-opt.txt && \
    pip install --cache-dir /cache/pip -r /tmp/requirements-test.txt

RUN chown -R test_user:test_user /cache && \
    chown -R test_user:test_user /tmp

# create virtualenv & install ruby dependencies
USER test_user
WORKDIR /home/test_user/repo
ENV BASH_ENV '/home/test_user/.bashrc'
RUN echo 'set -o vi' | tee -a /home/test_user/.bashrc && \
    echo "bind 'set show-all-if-ambiguous on'" | tee -a /home/test_user/.bashrc && \
    echo "bind 'TAB:menu-complete'" | tee -a /home/test_user/.bashrc && \
    echo 'PATH="$HOME/bin:$PATH"' | tee -a /home/test_user/.bashrc && \
    #echo "PATH=$PATH:$(ruby -rubygems -e 'puts Gem.user_dir')/bin" | tee -a /home/test_user/.bashrc && \
    echo 'gem: --no-rdoc --no-ri' | tee -a /home/test_user/.gemrc && \
    mkdir -p /home/test_user/repo && \
    cp /tmp/Gemfile /home/test_user/Gemfile && \
    bundle install --path /cache/bundle --gemfile /home/test_user/Gemfile --binstubs ~/bin

ENTRYPOINT ["/home/test_user/bin/rake"]
