# (C) Fractal Industries, Inc. 2016
# (C) Datadog, Inc. 2010-2016
# All rights reserved
# Licensed under Simplified BSD License (see LICENSE)

require './ci/common'

# FIXME: use our own brew of MySQL like other flavors

namespace :ci do
  namespace :mysql do |flavor|
    task before_install: ['ci:common:before_install']

    task install: ['ci:common:install'] do
      sh %(sudo -E apt-get -q -y install mysql-server)
      sh %(sudo service mysql start || sudo service mysql restart)
    end

    task before_script: ['ci:common:before_script'] do
      sh %(sudo mysql -e "create user 'dog'@'localhost' identified by 'dog'")
      sh %(sudo mysql -e "GRANT PROCESS, REPLICATION CLIENT ON *.* TO 'dog'@'localhost' WITH MAX_USER_CONNECTIONS 5;")
      sh %(sudo mysql -e "CREATE DATABASE testdb;")
      sh %(sudo mysql -e "CREATE TABLE testdb.users (name VARCHAR(20), age INT);")
      sh %(sudo mysql -e "GRANT SELECT ON testdb.users TO 'dog'@'localhost';")
      sh %(sudo mysql -e "INSERT INTO testdb.users (name,age) VALUES('Alice',25);")
      sh %(sudo mysql -e "INSERT INTO testdb.users (name,age) VALUES('Bob',20);")
      sh %(sudo mysql -e "GRANT SELECT ON performance_schema.* TO 'dog'@'localhost';")
      sh %(sudo mysql -e "USE testdb; SELECT * FROM users ORDER BY name;")
    end

    task script: ['ci:common:script'] do
      this_provides = [
        'mysql'
      ]
      Rake::Task['ci:common:run_tests'].invoke(this_provides)
    end

    task before_cache: ['ci:common:before_cache']

    task cleanup: ['ci:common:cleanup'] do
      sh %(sudo mysql -e "DROP USER 'dog'@'localhost';")
      sh %(sudo mysql -e "DROP DATABASE testdb;"t)
    end

    task :execute do
      Rake::Task['ci:common:execute'].invoke(flavor)
    end
  end
end
