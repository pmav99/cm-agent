#/usr/bin/env bash

set -e

if [ $# -eq 0 ]
then
    docker run \
        --rm \
        -v $PWD:/home/test_user/repo \
        -v cm-cache:/cache \
        --entrypoint /bin/bash \
        --user root \
        -it cm-local
else
    docker run \
        --rm \
        -v $PWD:/home/test_user/repo \
        -v cm-cache:/cache \
        -e FLAVOR_VERSION=1.6.2 \
        -it cm-local $@
fi
