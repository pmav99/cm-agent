#!/bin/sh
# (C) Fractal Industries, Inc. 2016
# (C) Datadog, Inc. 2010-2016
# All rights reserved
# Licensed under Simplified BSD License (see LICENSE)

PATH=/opt/conmon-agent/embedded/bin:/opt/conmon-agent/bin:$PATH

exec /opt/conmon-agent/bin/supervisord -c /etc/cm-agent/supervisor.conf
