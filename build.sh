#/usr/bin/env bash

set -e

# in order to speed things up we are using a cache on data volume.
# We will need to create this cache the first time we create the
# container and we must also make sure that the data volume is
# writable by `test_user`.
if [ "$(docker volume ls | grep -c cm-cache)" -ge 1 ]; then
    docker volume create --name cm-cache
    docker run\
        --rm \
        -v cm-cache:/cache \
        -it ubuntu chown -R 1000:1000 /cache
fi

# build the docker image
docker build -t cm-local ./
